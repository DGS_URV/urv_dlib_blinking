// The contents of this file are in the public domain. See LICENSE_FOR_EXAMPLE_PROGRAMS.txt
/*

This example program shows how to find frontal human faces in an image and
estimate their pose.  The pose takes the form of 68 landmarks.  These are
points on the face such as the corners of the mouth, along the eyebrows, on
the eyes, and so forth.


This example is essentially just a version of the face_landmark_detection_ex.cpp
example modified to use OpenCV's VideoCapture object to read from a camera instead
of files.


Finally, note that the face detector is fastest when compiled with at least
SSE2 instructions enabled.  So if you are using a PC with an Intel or AMD
chip then you should enable at least SSE2 instructions.  If you are using
cmake to compile this program you can enable them by using one of the
following commands when you create the build project:
cmake path_to_dlib_root/examples -DUSE_SSE2_INSTRUCTIONS=ON
cmake path_to_dlib_root/examples -DUSE_SSE4_INSTRUCTIONS=ON
cmake path_to_dlib_root/examples -DUSE_AVX_INSTRUCTIONS=ON
This will set the appropriate compiler options for GCC, clang, Visual
Studio, or the Intel compiler.  If you are using another compiler then you
need to consult your compiler's manual to determine how to enable these
instructions.  Note that AVX is the fastest but requires a CPU from at least
2011.  SSE4 is the next fastest and is supported by most current machines.
*/


#include "blink.h"
#include <dlib/opencv.h>
#include <opencv2/highgui/highgui.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
//#include <dlib/gui_widgets.h>
#include <time.h>


using namespace dlib;
using namespace std;

float  EAR_izq, EAR_der;
clock_t start_timer = 0, calibrating_timer = 0;
float threshold = 0;
boolean accion = false, cerrado = false, calibrated = false;

float EAR_der_max = 0;
float EAR_der_min = 0;
float EAR_izq_max = 0;
float EAR_izq_min = 0;

float remaining_time;

double Distance(double dX0, double dY0, double dX1, double dY1) {

	return sqrt((dX1 - dX0) * (dX1 - dX0) + (dY1 - dY0) * (dY1 - dY0));
}

float calibrate_threshold(float EAR_der, float EAR_izq) {


	/*float EAR_der_max = 0;
	float EAR_der_min = 0;
	float EAR_izq_max = 0;
	float EAR_izq_min = 0;*/
	float EAR;
	float EAR_izq_temp, EAR_der_temp;

	remaining_time = (clock() - calibrating_timer) / CLOCKS_PER_SEC;

	if (remaining_time > 5)
	{
		calibrated = true;

		//Calcular threshold derecha al 70%
		EAR_der_temp = EAR_der_max - EAR_der_min;
		EAR_der_temp = (EAR_der_temp * 0.7) + EAR_der_min;

		//Calcular threshold izquierda al 70%
		EAR_izq_temp = EAR_izq_max - EAR_izq_min;
		EAR_izq_temp = (EAR_der_temp * 0.7) + EAR_izq_min;

		//Calcular media
		EAR = (EAR_der_temp + EAR_izq_temp) / 2;



	}
	else
	{
		if (EAR_der_max < EAR_der)
			EAR_der_max = EAR_der;

		else if (EAR_der_min > EAR_der)
			EAR_der_min = EAR_der;

		else if (EAR_izq_max < EAR_izq)
			EAR_izq_max = EAR_izq;

		else if (EAR_izq_min > EAR_izq)
			EAR_izq_min = EAR_izq;
	}


	return EAR;
}

float calculateEAR(point p1, point p2, point p3, point p4, point p5, point p6) {

	float EAR;
	float unit1, unit2, unitario, determinante;

	/*point p1 = pose_model(cimg, faces[i]).part(36); //p1
	//int p1x = p1.x;
	//	int p1y = p1.y;

	point p2 = pose_model(cimg, faces[i]).part(37); //p2
	point p3 = pose_model(cimg, faces[i]).part(38); //p3
	point p4 = pose_model(cimg, faces[i]).part(39); //p4
	point p5 = pose_model(cimg, faces[i]).part(40); //p5
	point p6 = pose_model(cimg, faces[i]).part(41); //p6*/


	unit1 = Distance(p2.x(), p2.y(), p6.x(), p6.y());
	unit2 = Distance(p3.x(), p3.y(), p5.x(), p5.y());

	unitario = unit1 + unit2;

	determinante = 2 * Distance(p1.x(), p1.y(), p4.x(), p4.y());

	EAR = unitario / determinante;

	return EAR;
}

boolean checkClosed(float eye_threshold, int seconds_to_go, float EAR_izq, float EAR_der) {

	//float EAR = (EAR_izq + EAR_der) / 2;

	if ((EAR_der > eye_threshold) || (EAR_izq > eye_threshold)) {

		cerrado = false;
		accion = false;
		cout << "EAR: " << EAR_der << " Abierto \n";
	}
	else if (((EAR_der <= eye_threshold) || (EAR_izq <= eye_threshold)) && (!cerrado)) {
		cerrado = true;

		cout << "EAR: " << EAR_der << " Cerrado...ESPERANDO \n";
		start_timer = clock();
	}
	else if (cerrado) {

		float tiempo_pasado = (clock() - start_timer) / CLOCKS_PER_SEC;

		cout << "tIEMPO PASADO: " << tiempo_pasado << "  \n";
		//cout << "CERRADOOOOO: " << "  \n";
		if ((!accion) && (tiempo_pasado > seconds_to_go)) {
			cout << "CERRADOOOOO: " << "  \n";
			accion = true;

		}

	}
	return accion;
}

int main()
{
	try
	{
		cv::VideoCapture cap(0);
		if (!cap.isOpened())
		{
			cerr << "Unable to connect to camera" << endl;
			return 1;
		}

		image_window win;
		blink blink;
		//= new blink();

		/*// Load face detection and pose estimation models.
		frontal_face_detector detector = get_frontal_face_detector();
		shape_predictor pose_model;
		deserialize("shape_predictor_68_face_landmarks.dat") >> pose_model;

		// Grab and process frames until the main window is closed by the user.
		calibrating_timer = clock(); //timer para calibrar EAR*/

		while (!win.is_closed())
		{
			// Grab a frame
			cv::Mat temp;
			cap >> temp;

			bool paco = blink.check_blinking(cap);

			cout << " EM MAIN: " << paco << "  \n";
			// Turn OpenCV's Mat into something dlib can deal with.  Note that this just
			// wraps the Mat object, it doesn't copy anything.  So cimg is only valid as
			// long as temp is valid.  Also don't do anything to temp that would cause it
			// to reallocate the memory which stores the image as that will make cimg
			// contain dangling pointers.  This basically means you shouldn't modify temp
			// while using cimg.
			/*cv_image<bgr_pixel> cimg(temp);

			// Detect faces
			std::vector<rectangle> faces = detector(cimg);
			// Find the pose of each face.
			std::vector<full_object_detection> shapes;
			for (unsigned long i = 0; i < faces.size(); ++i){


			shapes.push_back(pose_model(cimg, faces[i]));

			//Coordenadas ojo derecho. Landmarks 36 a 41
			point p1 = pose_model(cimg, faces[i]).part(36); //p1; int p1x = p1.x; int p1y = p1.y;
			point p2 = pose_model(cimg, faces[i]).part(37); //p2
			point p3 = pose_model(cimg, faces[i]).part(38); //p3
			point p4 = pose_model(cimg, faces[i]).part(39); //p4
			point p5 = pose_model(cimg, faces[i]).part(40); //p5
			point p6 = pose_model(cimg, faces[i]).part(41); //p6

			//Coordenadas ojo izquierdo. Landmarks 42 a 47
			point p7 = pose_model(cimg, faces[i]).part(42); //p1
			point p8 = pose_model(cimg, faces[i]).part(43); //p2
			point p9 = pose_model(cimg, faces[i]).part(44); //p3
			point p10 = pose_model(cimg, faces[i]).part(45); //p4
			point p11 = pose_model(cimg, faces[i]).part(46); //p5
			point p12 = pose_model(cimg, faces[i]).part(47); //p6


			/*	unit1 = Distance(p2.x(), p2.y(), p6.x(), p6.y());
			unit2 = Distance(p3.x(), p3.y(), p5.x(), p5.y());

			unitario = unit1 + unit2;

			determinante = 2 * Distance(p1.x(), p1.y(), p4.x(), p4.y());

			EAR = unitario / determinante;*/
			/*	EAR_der = calculateEAR(p1, p2, p3, p4, p5, p6);
			EAR_izq = calculateEAR(p7, p8, p9, p10, p11, p12);

			if (!calibrated)
			threshold = calibrate_threshold(EAR_der, EAR_izq);
			else
			checkClosed(threshold,2, EAR_izq, EAR_der); //threshold and seconds to count until click
			//cout << EAR << " \n";
			//system("cls");
			/*	if (EAR > 0.15) {

			cerrado = false;
			accion = false;
			//cout << "EAR: " << EAR << " Abierto \n";
			}
			else if ((EAR <= 0.15) && (!cerrado)) {
			cerrado = true;

			cout << "EAR: " << EAR << " Cerrado...ESPERANDO \n";
			start_timer = clock();
			}
			else if (cerrado) {

			float tiempo_pasado = (clock() - start_timer) / CLOCKS_PER_SEC;

			cout << "tIEMPO PASADO: " << tiempo_pasado << "  \n";

			if ((!accion) && ( tiempo_pasado > 3)){
			cout << "CERRADOOOOO: " << "  \n";
			accion = true;

			}

			}*/




			/*	float unit1 = pose_model(cimg, faces[i]).part(37)(41);

			//	cout <<  "\n unit1: " << unit1 ;

			float unit2 = pose_model(cimg, faces[i]).part(38)(40);

			//cout << "\n unit2: " << unit2;

			float unitario = unit1 + unit2;

			//	cout << "\n unitario: " << unitario;

			float determinante = 2 * (pose_model(cimg, faces[i]).part(36)(39));

			//	cout << "\n determinante: " << determinante;

			float EAR = unitario / determinante;

			cout << "\n EAR: " << EAR;

			*/

			/*	double unitario = norm(p2 - p6) + norm(p3 - p5);
			double determinante = (2 * norm(p1 - p4));
			double EAR = unitario / determinante;*/


			//	((norm(pose_model(cimg, faces[i]).part(36)) - norm(pose_model(cimg, faces[i]).part(39))));


			/*}



			// Display it all on the screen
			win.clear_overlay();
			win.set_image(cimg);
			win.add_overlay(render_face_detections(shapes));*/
		}
	}



	catch (serialization_error& e)
	{
		cout << "You need dlib's default face landmarking model file to run this example." << endl;
		cout << "You can get it from the following URL: " << endl;
		cout << "   http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2" << endl;
		cout << endl << e.what() << endl;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}


}

